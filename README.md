# Lab5 -- Integration testing

## Specs

        Here is InnoCar Specs:

        Budet car price per minute = 15

        Luxury car price per minute = 37

        Fixed price per km = 12

        Allowed deviations in % = 19

        Inno discount in % = 14.000000000000002

## BVA table


| Parameter   | Equivalence Classes      |
|------------|--------------------------|
| plan       |minute, fixed_price, nonsence |
| type       |budget, luxury, nonsence |
| distance   |<=0, >0, empty |
| time       |<=0, >0, empty |
| planned_distance|<=0, >0, empty|
| planned_time|<=0, >0, empty|
| inno_discount|yes, no, nonsence |

## Desicion table

|#|  Type  |  Plan  |  Distance  |   Time  |    Planned_distance  |    Planned_time  |    Inno_discount  | Status |
|--|--------|--------|--------|--------|--------|--------|--------|--------|
|1| nonsense | * | * | * | * | * | * | Invalid|
|2| * | nonsense | * | * | * | * | * | Invalid|
|3| * | * | empty | * | * | * | * | Invalid|
|4| * | * | <=0 | * | * | * | * | Invalid|
|5| * | * | * | empty | * | * | * | Invalid|
|6| * | * | * | <=0 | * | * | * | Invalid|
|7| * | * | * | * | empty | * | * | Invalid|
|8| * | * | * | * | <=0 | * | * | Invalid|
|9| * | * | * | * | * | empty | * | Invalid|
|10| * | * | * | * | * | <=0 | * | Invalid|
|11| * | * | * | * | * | * | nonsense | Invalid|
|12|luxury|fixed_price|*|*|*|*|*|Invalid|
|13|luxury|minute|>0|>0|>0|>0|yes|Valid|
|14|luxury|minute|>0|>0|>0|>0|no|Valid|
|15|budget|fixed_price|>0|>0|>0|>0|yes|Valid|
|16|budget|fixed_price|>0|>0|>0|>0|no|Valid|
|17|budget|minute|>0|>0|>0|>0|yes|Valid|
|18|budget|minute|>0|>0|>0|>0|no|Valid|

## Tests

DT Entry |  Type  |  Plan  |  Distance  |   Planned_distance  |    Time  |    Planned_time  |    Inno_discount  | Expected | Got | Summary |
|-|--------|--------|--------|--------|--------|--------|--------|--------|--------|--------|
|1|nonsense|fixed_price|10|10|1|1|yes|Invalid Request|{"price":107.5}|<span style="color:red">__Domain Error__</span>|
|2|budget|nonsense|10|10|1|1|yes|Invalid Request|Invalid Request|<span style="color:green">__OK__</span>|
|3|budget|fixed_price||10|1|1|yes|Invalid Request|{"price":16.6}|<span style="color:red">__Domain Error__</span>|
|4|budget|minute|-10|10|1|1|yes|Invalid Request|Invalid Request|<span style="color:green">__OK__</span>|
|5|budget|minute|10||1|1|yes|Invalid Request|{"price":12.9}|<span style="color:red">__Domain Error__</span>|
|6|budget|minute|10|-10|1|1|yes|Invalid Request|Invalid Request|<span style="color:green">__OK__</span>|
|7|budget|minute|10|10||1|yes|Invalid Request|{"price":0}|<span style="color:red">__Domain Error__</span>|
|8|budget|minute|10|10|-1|1|yes|Invalid Request|Invalid Request|<span style="color:green">__OK__</span>|
|9|budget|minute|10|10|1||yes|Invalid Request|{"price":12.9}|<span style="color:red">__Domain Error__</span>|
|10|budget|minute|10|10|1|-1|yes|Invalid Request|{"price":12.9}|<span style="color:red">__Domain Error__</span>|
|11|budget|minute|10|10|1|1|nonsense|Invalid Request|Invalid Request|<span style="color:green">__OK__</span>|
|12|luxury|fixed_price|10|10|1|1|yes|Invalid Request|Invalid Request|<span style="color:green">__OK__</span>|
|13|luxury|minute|10|10|1|1|yes|{"price":31.82}|{"price":31.82}|<span style="color:green">__OK__</span>|
|14|luxury|minute|10|10|1|1|no|{"price":37}|{"price":37}|<span style="color:green">__OK__</span>|
|15|budget|fixed_price|10|10|1|1|yes|{"price":103.2}|{"price":107.5}|<span style="color:red">__Computational Error__</span>|
|15|budget|fixed_price|20|10|1|1|yes|{"price":12.9}|{"price":14.3}|<span style="color:red">__Computational Error__</span>|
|15|budget|fixed_price|20|10|1|1|no|{"price":15}|{"price":16.6}|<span style="color:red">__Computational Error__</span>|
|15|budget|fixed_price|20|10|2|1|yes|{"price":25.8}|{"price":28.6}|<span style="color:red">__Computational Error__</span>|
|15|budget|fixed_price|20|10|2|1|no|{"price":30}|{"price":33.3}|<span style="color:red">__Computational Error__</span>|
|16|budget|fixed_price|10|10|1|1|no|{"price":120}|{"price":125}|<span style="color:red">__Computational Error__</span>|
|17|budget|minute|10|10|1|1|yes|{"price":12.9}|{"price":12.9}|<span style="color:green">__OK__</span>|
|18|budget|minute|10|10|1|1|no|{"price":15}|{"price":15}|<span style="color:green">__OK__</span>|

## Bugs

1. Type doesn't not checked correctly
2. Fields are not checked for the emptiness
3. Planned time is not checked for being <0
4. Fixed price is not calculated correctly

Estimated coverage = 80%
